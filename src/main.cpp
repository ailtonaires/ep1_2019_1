#include <iostream>
#include <string>
#include <fstream>
#include "mapa.hpp"
#include "embarcacao.hpp"

using namespace std;

int main(){

	int option;
	string nome_mapa;
	ofstream mapa_1;
	ofstream mapa_2;
	ifstream mapa;
	fstream pontuacao;

	pontuacao.open("doc/pontuacao.txt", ios::in);


	if(pontuacao.fail()==1){

		pontuacao.open("doc/pontuacao.txt", ios::app);
		pontuacao<<"0 0";

	}

	mapa_1.open("doc/mapa_1.txt", ios::trunc);
	mapa_1.close();										//Criação dos arquivos, caso não existam
	mapa_1.open("doc/mapa_2.txt", ios::trunc);
	mapa_1.close();
	pontuacao.close();

	Mapa jogador_1;
	Mapa jogador_2;
	Mapa partida;
	Mapa filtrar;


	system("clear");



	cout<<"Escolha a opção que deseja jogar:"<<endl<<endl;

	cout<<"1) Mapa pré-definido\n2) Posicionar embarcações no mapa\n3) Pontuação atual dos jogadores"<<endl<<endl;
	cout<<"Opção: ";

	do{
		cin>>option;

	}while(option<1 || option>3);

	switch (option){

		case 1:

		do{
			system("clear");

			cout<<"Informe o nome do mapa que deseja utilizar: ";
			cin>>nome_mapa;			

			nome_mapa = "doc/"+nome_mapa+".txt";
			mapa.open(nome_mapa, ios::in);
			mapa.close();

		}while(mapa.fail()==1);

		filtrar.set_mapa_estatico(nome_mapa);

		filtrar.~Mapa();		

		break;

		case 2:

		system("clear");

		jogador_1.set_localizacao(1);
		jogador_1.~Mapa();
		jogador_2.set_localizacao(2);
		jogador_2.~Mapa();
		partida.set_localizacao("mapa_2");
		partida.~Mapa();
		break;

		case 3:
		Mapa points;
		points.set_pontuacao(0, 0);
		points.~Mapa();
		break;
	}

	return 0;

}
