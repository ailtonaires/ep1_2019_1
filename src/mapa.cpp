#include <string>
#include <iostream>
#include <fstream>
#include <cstring>
#include <stdlib.h>
#include "mapa.hpp"
#include "porta_avioes.hpp"
#include "submarino.hpp"
#include "embarcacao.hpp"
#include "canoa.hpp"



using namespace std;

Mapa::Mapa(){


}

Mapa::~Mapa(){
    //cout << "Destrutor da classe arquivo" << endl;
}



void Mapa::set_localizacao(int player){

	char mapa[13][13];
	int i,j,k=1;
	int linha;
	int coluna;
	int direcao;

	for(i=0;i<13;i++){
		for (j=0; j<13; j++){
			mapa[i][j] = 48;
		}

	}

	ofstream mapa_2;

	mapa_2.open("doc/mapa_2.txt", ios::app);


	ofstream mapa_1;

	mapa_1.open("doc/mapa_1.txt", ios::app);
	mapa_1 << "\n#Player_"<<player<< endl; 

	cout << "  PLAYER "<<player<<" \n\nInforme a linha e coluna, respectivamente, que deseja colocar suas canoas."<<endl;

	for (i=0; i<6; i++){

		do{
			cout <<i+1<<"ª canoa "<<endl;

			cout << "Linha: ";
			cin >> linha;
			cout << "\nColuna: ";
			cin >> coluna;

			while (mapa[linha][coluna]!= 48){

				system("clear");
				cout<<"Já existe uma embarcação nesta posição. Escolha outra."<<endl<<endl;
				cout << "Linha: ";
				cin >> linha;
				cout << "\nColuna: ";
				cin >> coluna;
			}

			mapa[linha][coluna]= 99;

		}while(linha>12 || coluna>>12);



		mapa_1 << linha <<" "<< coluna <<" canoa "<<"nada"<< endl; 
		mapa_2 << linha <<" "<< coluna <<" canoa "<<"nada"<< endl; 


	}

	mapa_1<<"\n";

	system("clear");

	cout << "Informe a linha, coluna e direção, respectivamente, que deseja colocar seus submarinos."<<endl;

	for (i=0; i<4; i++){

		cout <<i+1<<"º submarino "<<endl;
		do{
			cout << "Linha: ";
			cin >> linha;
			cout << "\nColuna: ";
			cin >> coluna;
			cout<<"Direção:\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo "<<endl;


		}while(linha > 12 || coluna > 12);
		
		do{


			cout<<"Por favor, digite um número correspondente a opção desejada: ";
			cin>>direcao;
			
		}while( direcao != 1 && direcao != 2 && direcao != 3 && direcao != 4);


		while (mapa[linha][coluna]!= 48){

			cout<<"Já existe uma embarcação nesta posição. Escolha outra."<<endl<<endl;
			cout << "Linha: ";
			cin >> linha;
			cout << "\nColuna: ";
			cin >> coluna;
			cout<<"\nDireção:\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo "<<endl;
			cin>>direcao;

		}

		DIRECAO:

		switch(direcao){



			case 1:

			if(mapa[linha][coluna-k]!= 48){

				system("clear");

				cout<<"Já existe uma embarcação nesta direção. Por favor, digite uma nova direção."<<endl<<endl;

				cout<<"\nDireção:\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo "<<endl;
				cin>>direcao;

				goto DIRECAO;
				

			}

			mapa[linha][coluna] = 115;
			mapa[linha][coluna-k] = 115;

			mapa_1 << linha <<" "<< coluna <<" submarino "<< "esquerda" << endl; 
			mapa_2 << linha <<" "<< coluna <<" submarino "<< "esquerda" << endl; 

			break;

			case 2:

			if(mapa[linha][coluna+k]!= 48){

				system("clear");

				cout<<"Já existe uma embarcação nesta direção. Por favor, digite uma nova direção."<<endl<<endl;

				cout<<"\nDireção:\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo "<<endl;
				cin>>direcao;

				goto DIRECAO;

			}

			mapa[linha][coluna] = 115;
			mapa[linha][coluna+k] = 115;
			mapa_1 << linha <<" "<< coluna <<" submarino "<< "direita" << endl; 
			mapa_2 << linha <<" "<< coluna <<" submarino "<< "direita" << endl; 

			break;

			case 3:


			if(mapa[linha-k][coluna]!= 48){

				system("clear");

				cout<<"Já existe uma embarcação nesta direção. Por favor, digite uma nova direção."<<endl<<endl;

				cout<<"\nDireção:\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo "<<endl;
				cin>>direcao;

				goto DIRECAO;
			}

			mapa[linha][coluna] = 115;
			mapa[linha-k][coluna] = 115;
			mapa_1 << linha <<" "<< coluna <<" submarino "<< "cima" << endl; 
			mapa_2 << linha <<" "<< coluna <<" subamrino "<< "cima" << endl; 

			break;

			case 4:
			if(mapa[linha+k][coluna]!= 48){

				system("clear");

				cout<<"Já existe uma embarcação nesta direção. Por favor, digite uma nova direção."<<endl<<endl;

				cout<<"\nDireção:\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo "<<endl;

				cin>>direcao;

				goto DIRECAO;
				
			}

			mapa[linha][coluna] = 115;
			mapa[linha+k][coluna] = 115;
			mapa_1 << linha <<" "<< coluna <<" submarino "<< "baixo" << endl; 
			mapa_2 << linha <<" "<< coluna <<" submarino "<< "baixo" << endl; 

			break;

		}



	}

	mapa_1<<"\n";
	system("clear");

	for (i=0; i<2; i++){

		
		do{	

			system("clear");

			cout <<i+1<<"º porta avião "<<endl;
			cout << "Linha: ";
			cin >> linha;
			cout << "\nColuna: ";
			cin >> coluna;
			cout<<"\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo\n\nDireção:"<<endl;
			cin>>direcao;

		}while(linha>12 || coluna>12);

		while (mapa[linha][coluna]!= 48){

			cout<<"Já existe uma embarcação nesta posição. Escolha outra."<<endl<<endl;
			cout << "Linha: ";
			cin >> linha;
			cout << "\nColuna: ";
			cin >> coluna;
			cout<<"\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo\n\nDireção:"<<endl;
			cin>>direcao;

		}


		while( direcao != 1 && direcao != 2 && direcao != 3 && direcao != 4){

			cout<<"\nDireção:\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo "<<endl;
			cout<<"Por favor, digite um número correspondente a opção desejada: ";
			cin >> direcao;
		}

		DIRECAO2:

		switch(direcao){

			case 1:

			if (mapa[linha][coluna-1]!= 48 || mapa[linha][coluna-2]!= 48 || mapa[linha][coluna-3]!= 48){

				system("clear");

				cout<<"Já existe uma embarcação nesta direção. Por favor, digite uma nova direção."<<endl<<endl;

				cout<<"\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo\n\nDireção: "<<endl;

				cin>>direcao;

				goto DIRECAO2;

			}

			mapa[linha][coluna]= 112; mapa[linha][coluna-1]= 112; mapa[linha][coluna-2]= 112; mapa[linha][coluna-3]= 112;
			mapa_1 << linha <<" "<< coluna <<" porta-avioes "<< "esquerda" << endl; 
			mapa_2 << linha <<" "<< coluna <<" porta-avioes "<< "direita" << endl; 
			break;

			case 2:

			if (mapa[linha][coluna+1]!= 48 || mapa[linha][coluna+2]!= 48 || mapa[linha][coluna+3]!= 48){

				system("clear");

				cout<<"Já existe uma embarcação nesta direção. Por favor, digite uma nova direção."<<endl<<endl;

				cout<<"\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo\n\nDireção: "<<endl;

				cin>>direcao;

				goto DIRECAO2;

			}

			mapa[linha][coluna]= 112; mapa[linha][coluna+1]= 112; mapa[linha][coluna+2]= 112; mapa[linha][coluna+3]= 112;
			mapa_1 << linha <<" "<< coluna <<" porta-avioes "<< "direita" << endl; 
			mapa_2 << linha <<" "<< coluna <<" porta-avioes "<< "direita" << endl; 

			break;

			case 3:

			if (mapa[linha-1][coluna]!= 48 || mapa[linha-2][coluna]!= 48 || mapa[linha-3][coluna]!= 48){

				system("clear");

				cout<<"Já existe uma embarcação nesta direção. Por favor, digite uma nova direção."<<endl<<endl;

				cout<<"\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo\n\nDireção: "<<endl;

				cin>>direcao;

				goto DIRECAO2;

			}

			mapa[linha][coluna]= 112; mapa[linha-1][coluna]= 112; mapa[linha-2][coluna]= 112; mapa[linha-3][coluna]= 112;
			mapa_1 << linha <<" "<< coluna <<" porta-avioes "<< "cima" << endl;
			mapa_2 << linha <<" "<< coluna <<" porta-avioes "<< "cima" << endl; 

			break;

			case 4:

			if (mapa[linha+1][coluna]!= 48 || mapa[linha+2][coluna]!= 48 || mapa[linha+3][coluna]!= 48){

				system("clear");

				cout<<"Já existe uma embarcação nesta direção. Por favor, digite uma nova direção."<<endl<<endl;

				cout<<"\n\n1)Esquerda\n2)Direita\n3)Cima\n4)Baixo\n\nDireção: "<<endl;

				cin>>direcao;

				goto DIRECAO2;

			}

			mapa[linha][coluna]= 112; mapa[linha+1][coluna]= 112; mapa[linha+2][coluna]= 112; mapa[linha+3][coluna]= 112;

			mapa_1 << linha <<" "<< coluna <<" porta-avioes "<< "baixo" << endl; 
			mapa_2 << linha <<" "<< coluna <<" porta-avioes "<< "baixo" << endl; 

			break;

		}

	}
	system("clear");

	

	mapa_1.close();	
	mapa_2.close();

}

void Mapa::set_localizacao(string map){

	map = "doc/"+map+".txt";

	int i, cont=0;
	string x, y, embarcacao, direcao;
	int linha;
	int coluna;
	char mapa[13][13];
	char mapa2[13][13];
	
	memset(mapa, 48, sizeof(mapa));
	memset(mapa2,48, sizeof(mapa2));

	ifstream mapa_2;

	mapa_2.open(map, ios::in);

	if (mapa_2.is_open() && mapa_2.good()){

		while(!mapa_2.fail()){
			

			mapa_2 >> x >> y >> embarcacao >> direcao;

			linha = std::stoi(x);
			coluna = std::stoi(y);

			if(embarcacao == "canoa"){

				if(cont<12){

					mapa[linha][coluna] = 99;

				}else{

					mapa2[linha][coluna] = 99;

				}

			}

			if (embarcacao == "submarino"){


				if(cont<12){

					mapa[linha][coluna] = 115;

				}else{

					mapa2[linha][coluna] = 115;

				}
				switch(direcao[0]){

					case 100:

					if(cont<12){

						mapa[linha][coluna+1] = 115;

					}else{

						mapa2[linha][coluna+1] = 115;

					}

					break;

					case 101:

					if(cont<12){

						mapa[linha][coluna-1] = 115;

					}else{

						mapa2[linha][coluna-1] = 115;

					}
					break;

					case 99:

					if(cont<12){

						mapa[linha-1][coluna] = 115;

					}else{

						mapa2[linha-1][coluna] = 115;
					}
					break;

					case 98:

					if(cont<12){

						mapa[linha+1][coluna] = 115;

					}else{

						mapa2[linha+1][coluna] = 115;

					}

					break;
				}
			}


			if (embarcacao == "porta-avioes"){

				switch(direcao[0]){

					case 100:
					for(i=0;i<4;i++){

						if(cont<12){

							mapa[linha][coluna+i] = 112;
						}else{

							mapa2[linha][coluna+i] = 112;

						}

					}
					break;

					case 101:


					for(i=0;i<4;i++){

						if(cont<12){

							mapa[linha][coluna-i] = 112;
						}else{

							mapa2[linha][coluna-i] = 112;

						}

					}

					break;

					case 99:

					for(i=0;i<4;i++){

						if(cont<12){

							mapa[linha-i][coluna] = 112;

						}else {

							mapa2[linha-i][coluna] = 112;

						}
					}

					break;

					case 98:

					for(i=0;i<4;i++){
						if(cont<12){

							mapa[linha+i][coluna] = 112;

						}else{

							mapa2[linha+i][coluna] = 112;

						}
					}

					break;
				}


			}


			cont++;
		}

	}

	mapa_2.close();	


	set_partida( mapa, mapa2 );


}








void Mapa::set_mapa_estatico(string nome_mapa){

	string embarcacao, direcao;
	string linha;
	string x,y;
	int cont=0;

	ifstream mapa;
	fstream new_mapa;
	new_mapa.open("doc/new_mapa.txt", ios::out);
	mapa.open(nome_mapa, ios::in);

	while (getline(mapa, linha)){

		if(cont>12	){

			if(linha.size() != 0){
				if( linha!="# player_1" && linha!="# player_2" && linha[0]!='\n'){

					new_mapa<<linha<<endl;
				}
			}


		}


		cont++;
	}

	mapa.close();
	new_mapa.close();








	set_localizacao("new_mapa");

}


void Mapa::set_partida(char mapa[13][13], char mapa2[13][13]){

	Canoa canoa;
	Submarino sub;
	Porta_avioes port_avioes;

	int cont1=30, cont2 = 30;
	int linha, coluna;

	system("clear");


	while(cont1>0 && cont2>0){


		set_atualizar_mapa( mapa, mapa2, cont1, cont2);

		

		do {

			if(cont1==0 || cont2==0){
				break;

			}

			REPETIR1:

			cout<<"\n\n VEZ DO JOGADOR 1\n\n\n Informe a linha e coluna, respectivamente, que deseja lançar a bomba\n\n";

			cout<<" LINHA:";
			cin>>linha;
			cout<<" COLUNA:";
			cin>>coluna;



			if(mapa2[linha][coluna] == 88 || mapa2[linha][coluna] == 80 || mapa2[linha][coluna] == 83 || mapa2[linha][coluna] == 67 ){

				system("clear");
				set_atualizar_mapa( mapa, mapa2, cont1, cont2);
				cout<<"\n                         VOCÊ JÁ LANÇOU UMA BOMBA AQUI. REPITA A JOGADA!"<<endl<<endl;

				goto REPETIR1;

			}	else if (mapa2[linha][coluna]== 99){

				system("clear");

				cont1=canoa.toma_dano(cont1);
				cout<<"\n                                        Você atingiu uma Canoa!"<<endl<<endl;
				mapa2[linha][coluna] = 67;
				set_atualizar_mapa( mapa, mapa2, cont1, cont2);


			} else if(mapa2[linha][coluna]== 42){

				system("clear");

				sub.set_poder(42);
				cout<<"\n                                          Você atingiu um Submarino!"<<endl<<endl;
				
				mapa2[linha][coluna]=sub.get_poder();


				cont1 = sub.toma_dano(cont1);
				set_atualizar_mapa( mapa, mapa2, cont1, cont2);

			}else if(mapa2[linha][coluna]== 115){

				system("clear");

				cout<<" \n                        Você atingiu um submarino, mas ele precisa de duas bombas pra ser afundado."<<endl<<endl;


				cont1 = sub.toma_dano(cont1);

				mapa2[linha][coluna]= 42;


				set_atualizar_mapa( mapa, mapa2, cont1, cont2);


			}else if(mapa2[linha][coluna] == 112){

				system("clear");

				cont1 = port_avioes.toma_dano(cont1);

				if(port_avioes.get_randomico()<50){

					system("clear");
					cout<<"\n                        A bomba foi abatida, e a embarcação não foi atingida!"<<endl<<endl;
					//set_atualizar_mapa( mapa, mapa2, cont1, cont2);
					break;

				}else{
					system("clear");
					cout<<"\n                                         Você acertou um Porta-Aviões!"<<endl<<endl;
					mapa2[linha][coluna]= 80;

					set_atualizar_mapa( mapa, mapa2, cont1, cont2);

				}


			}else if(mapa2[linha][coluna] == 48){

				system("clear");
				cout<<"\n                                    Você não acertou nenhuma embarcação!"<<endl<<endl;
				mapa2[linha][coluna] = 88;

			}



		}while(mapa2[linha][coluna]!= 48 && mapa2[linha][coluna]!= 88);


		do {

			if(cont2==0 || cont1==0){
				break;

			}

			set_atualizar_mapa( mapa, mapa2, cont1, cont2);

			REPETIR2:

			

			cout<<"\n\n VEZ DO JOGADOR 2\n\n\n Informe a linha e coluna, respectivamente, que deseja lançar a bomba\n\n";

			cout<<"LINHA:";
			cin>>linha;
			cout<<"COLUNA:";
			cin>>coluna;


			if(mapa[linha][coluna] == 88 || mapa[linha][coluna] == 80 || mapa[linha][coluna] == 83 || mapa[linha][coluna] == 67){

				system("clear");
				set_atualizar_mapa( mapa, mapa2, cont1, cont2);
				cout<<"\n                         VOCÊ JÁ LANÇOU UMA BOMBA AQUI. REPITA A JOGADA!"<<endl<<endl;
				goto REPETIR2;

			}else if (mapa[linha][coluna]== 99){

				system("clear");

				cont2=canoa.toma_dano(cont2);
				cout<<"\n                                          Você atingiu uma Canoa!"<<endl<<endl;
				mapa[linha][coluna] = 67;


			} else if(mapa[linha][coluna]== 42){

				system("clear");

				sub.set_poder(42);
				cout<<"\n                                      Você atingiu um Submarino!"<<endl<<endl;

				mapa[linha][coluna]=sub.get_poder();


				cont2 = sub.toma_dano(cont2);

			}else if(mapa[linha][coluna]== 115){

				system("clear");

				cout<<"\n                   Você atingiu um submarino, mas ele precisa de duas bombas pra ser afundado."<<endl<<endl;


				cont2 = sub.toma_dano(cont2);

				mapa[linha][coluna]= 42;


			}else if(mapa[linha][coluna] == 112){

				system("clear");

				cont2 = port_avioes.toma_dano(cont2);

				if(port_avioes.get_randomico()<50){

					system("clear");
					cout<<"\n                        A bomba foi abatida, e a embarcação não foi atingida!"<<endl<<endl;
					break;

				}else{

					system("clear");

					cout<<"\n                         Você acertou um Porta-Aviões!"<<endl<<endl;
					mapa[linha][coluna]= 80;

				}



			}else if(mapa[linha][coluna] == 48){

				system("clear");

				mapa[linha][coluna] = 88;
				cout<<"\n                                    Você não acertou nenhuma embarcação!"<<endl<<endl;

			} 


		}while(mapa[linha][coluna]!= 48 && mapa[linha][coluna]!= 88);

		
	}


	if(cont1==0){

		system("clear");
		cout<<"\n			 PLAYER 1 GANHOU! PARABÉNS!!!"<<endl;
		set_pontuacao(1, 0);

	}else if(cont2==0){

		system("clear");
		cout<<"\n			 PLAYER 2 GANHOU! PARABÉNS!!!"<<endl;
		set_pontuacao(0, 1);
	}

	sub.~Submarino();
	canoa.~Canoa();			//Destrutores
	port_avioes.~Porta_avioes();
}


void Mapa::set_atualizar_mapa(char mapa[13][13], char mapa2[13][13], int cont1, int cont2){

	int i,j;

	cout<<"  RESTAM "<<cont2<<" LOCALIZAÇÕES NO MAPA DO PLAYER 1";
	cout<<"                  RESTAM "<<cont1<<" LOCALIZAÇÕES NO MAPA DO PLAYER 2"<<endl<<endl;

	cout<<"               MAPA  PLAYER 1                                              MAPA  PLAYER 2"<<endl;

	cout<<"\n    0  1  2  3  4  5  6  7  8  9 10 11 12                       0  1  2  3  4  5  6  7  8  9 10 11 12\n";

	

	for(i=0;i<13; i++){

		if (i<10){
			cout<<i<<"  ";
		}else{
			cout<<i<<" ";
		}
		for (j=0;j<13;j++){

			if((mapa[i][j]!= 48) && (mapa[i][j]== 115 || mapa[i][j]== 99 || mapa[i][j]== 112)){

				cout<<"[0]";

			}else{
				cout<<"["<<mapa[i][j]<<"]";
			}
		}
		cout<<"                  ";

		if (i<10){
			cout<<i<<"  ";
		}else{
			cout<<i<<" ";
		}

		for (j=0;j<13;j++){


			if((mapa2[i][j]!= 48) && (mapa2[i][j]== 115 || mapa2[i][j]== 99 || mapa2[i][j]== 112)){

				cout<<"[0]";

			}else{
				cout<<"["<<mapa2[i][j]<<"]";
			}
		}

		cout<<endl;
	}

}

void Mapa::set_pontuacao(int point_1, int point_2){

	char i, j;

	fstream pontuacao;

	pontuacao.open("doc/pontuacao.txt", ios::in);

	pontuacao >> i >> j; 

	point_1 += (i - '0');
	point_2 +=  (j - '0');

	pontuacao.close();

	

	pontuacao.open("doc/pontuacao.txt", ios::out);
	pontuacao<<point_1<<" "<<point_2;
	pontuacao.close();

		cout<<endl<<" 				PLACAR DOS JOGADORES"<<endl<<endl;
		cout<<"				Player 1: "<<point_1<<" vitória(s)."<<endl;
		cout<<"				Player 2: "<<point_2<<" vitória(s)."<<endl;

}

