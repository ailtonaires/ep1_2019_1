#ifndef MAPA_HPP
#define MAPA_HPP

#include <string>

using namespace std;

class Mapa {

private:

	int cont1, cont2;

	void set_partida(char mapa[13][13], char mapa2[13][13]);
	float get_partida();
	void set_atualizar_mapa(char mapa[13][13], char mapa2[13][13], int cont1, int cont2);
	string get_atualizar_mapa();


public:

	Mapa();
	~Mapa();

	void set_mapa_estatico(string nome_mapa);


	void set_localizacao(int player);
	void set_localizacao(string map);
	void set_pontuacao(int point_1, int point_2);




};

#endif