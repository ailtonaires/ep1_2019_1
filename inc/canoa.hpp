#ifndef CANOA_HPP
#define CANOA_AVIOES_HPP
#include "embarcacao.hpp"
#include <string>

using namespace std;

class Canoa : public Embarcacao {

private:

int decrementar (int cont);

public:
	Canoa();
	~Canoa();
	int toma_dano(int cont);


};

#endif