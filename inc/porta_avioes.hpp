#ifndef PORTA_AVIOES_HPP
#define PORTA_AVIOES_HPP
#include "embarcacao.hpp"
#include <string>

using namespace std;

class Porta_avioes : public Embarcacao {

private:
	int randomico;
	void set_randomico();
	

public:
	Porta_avioes();
	~Porta_avioes();

int get_randomico();

	int toma_dano(int cont);

};

#endif