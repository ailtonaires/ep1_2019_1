#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP
#include "embarcacao.hpp"
#include <string>

using namespace std;

class Submarino : public Embarcacao {

private:


	char poder;

public:

	Submarino();
	~Submarino();

	void set_poder(char poder);
	char get_poder();
	int toma_dano(int cont);

};

#endif