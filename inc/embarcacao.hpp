#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP

#include <string>

using namespace std;

class Embarcacao {

private:

	char localizacao[3];
	int tamanho;

	virtual int toma_dano(int cont) = 0;

public:

	Embarcacao();
	~Embarcacao();


};

#endif