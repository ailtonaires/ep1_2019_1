# EP1 - OO 2019.1 (UnB - Gama)

# Descrição do projeto

O projeto visa a elaboração de um jogo, comumente conhecido como 'Batalha Naval', onde sua estrutura deve 	seguir os padrões estipulados, ou seja, os Paradigmas de Orientação a Objetos.

O programa inicia na main, já com a criação dos mapas, caso o usuário queira posicionar suas embarcações no mapa. Existe também um menu para poder escolher entre um mapa já existente (os que foram disponibilizados), ou posicionar as embarcações, que serão salvas em um arquivo.

Os objetos criados do tipo Mapa servem para dar inicio ao programa, fazendo assim todas as manipulações necessárias.

# Classes principais:

* Mapa

-- Controla basicamente as funções principais, que mexe com a parte de manipulação de arquivos 
(set_mapa_estatico), que permite o usuário colocar suas embarcações no mapa (set_localização), que controla o método de execução da partida (set_partida) e a atualização constante do mapa no terminal (set_atualizar_mapa).


* Embarcacao

-- Traz consigo o método virtual abstrato (toma_dano) para definir, a partir dos métodos individuais em cada classe que herdou de Embarcação, se as mesmas serão atingidas.

# Classes Filhas, que herdam de Embarcacao:

- Canoa

-- Implementado o método próprio para a Canoa, a partir do método virtual abstrato (toma_dano) da Embarcacao e o método decrementar, para fazer o controle de quantas posições já foram atingidas no mapa adversário.

- Submarino

-- Implementado o método próprio para o Submarino, a partir do método virtual abstrato (toma_dano) da Embarcacao e o método set_poder, para fazer o controle de quantas posições já foram atingidas no mapa adversário.

- Porta_avioes

-- Implementado o método próprio para o Porta-aviões, a partir do método virtual abstrato (toma_dano) da Embarcacao e o método set_randômico e get_randômico, para saber se a embarcação será atingida e seu retorno;



# Execução

Primeiramente é apresentado 3 opções, no qual você poderá optar por posicionar as embarcações no mapa, utilizar um mapa pré-definido (os que foram disponibilizados) e ver a pontuação dos jogadores.

Caso deseje posicionar as embarcações, será solicitado as coordenadas respectivas àquela embarcação.
Caso utilize um mapa pré-definido, basta indicar o nome do mapa (sem .txt) e se o mesmo existir, o programa irá importar as embarcações e já dará início ao jogo.
Caso queira saber a pontuação dos jogadores, basta selecionar a opção 3.





## Observações

* Com o intuito de fazer um trabalho com mais opções, além daquelas exigidas, existe uma funcionalidade para os próprios jogadores posicionar as suas embarcações onde eles bem entenderem. Isso viabiliza e melhora em parte a experiência do usuário com o jogo e com aquilo que foi proposto, pois dá a liberdade estratégia aos jogadores.

* Como os arquivos disponibilizados estão escritos como map_1.txt.txt, map_2.txt.txt e map_3.txt.txt, na hora que for solicitado o nome do mapa pré-definido, será necessário colocar na entrada, por exemplo, 'map_1.txt'. Se fosse um arquivo suposicao.txt, bastava assim colocar como entrada 'suposicao'.